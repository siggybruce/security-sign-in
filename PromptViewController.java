import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.PasswordField;
import javafx.scene.media.AudioClip;
import java.io.File;
import java.security.MessageDigest;

public class PromptViewController
{
    @FXML private PasswordField passwordField;

    public void checkPassword() throws Exception
    {
        //Theholylight91#!
        String hash = "48F1B5AC29A30B5DB210A40D974427B1422245739E7DCF3450E1C6E3C7C6B654F2836028B9E5227FABE595F9741" +
                "BC893E755CD169692C75A1E387F8705848AA6";

        if (hash.equals(getHash(passwordField.getText())))
        {
            AudioClip soundClip = new AudioClip(new File("src/Access_Granted.wav").toURI().toString());
            soundClip.play();
            Parent root = FXMLLoader.load(getClass().getResource("DataView.fxml"));
            passwordField.getScene().setRoot(root);
        }
        else
        {
            AudioClip soundClip = new AudioClip(new File("src/Access_Denied.wav").toURI().toString());
            soundClip.play();
        }
    }
    private String getHash(String input) throws Exception
    {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
        byte[] bytes = messageDigest.digest(input.getBytes("UTF-8"));
        StringBuilder hash = new StringBuilder();

        for (int index = 0; index < bytes.length; index++)
        {
            hash.append(String.format("%02x", bytes[index]));
        }
        return hash.toString().toUpperCase();
    }
}